<?php

/*
*  rest_api_init
*  Custom /upload route
*
*  @since 3.0
*/

add_action( 'rest_api_init', function () {
   $my_namespace = 'instant-images';
   $my_endpoint = '/upload';
   register_rest_route( $my_namespace, $my_endpoint, 
      array(
         'methods' => 'POST',
         'callback' => 'upload_image',
      )
   );
});



/*
*  upload_image
*  Upload Image to /uploads directory
*
*  @param $request      $_POST
*  @return $response    json
*  @since 3.0

*/

function upload_image( WP_REST_Request $request ) {
   
	if (is_user_logged_in() && current_user_can( apply_filters('instant_images_user_role', 'edit_theme_options') )){   	
		error_reporting(E_ALL|E_STRICT);   	
   	
   	// Create /instant-images directory inside /uploads to temporarily store images
      $dir = INSTANT_IMG_UPLOAD_PATH;
      if(!is_dir($dir)){
			wp_mkdir_p($dir);
      }

      // Is directory writeable, if not exit with an error
      if (!is_writable(INSTANT_IMG_UPLOAD_PATH.'/')) {
         $json = json_encode(
         	array(
         		'error' => true,
         		'msg' => __('Unable to save image, check your server permissions of `uploads/instant-instants`', 'instant-images')
      		)
         );
      	wp_send_json($json);
      }
   	   	
   	
      $data = json_decode($request->get_body()); // Get contents of request     
      $id = sanitize_key($data->id); // Image ID
      $img = sanitize_text_field($data->image); // Image URL
      
      $path = INSTANT_IMG_UPLOAD_PATH.'/'; // Temp Image Path
      $url = INSTANT_IMG_UPLOAD_URL; // Full url path for image upload
      
      // Create temp. image variable
      $filename = $id.'.jpg';
      $tmp_img = $path .''.$filename;
      
      
      // Confirm cURL is enabled
      if(in_array  ('curl', get_loaded_extensions())) {

         // Generate temp image from URL and store it on server for upload
         $ch = curl_init(); // Lets use cURL
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
         curl_setopt($ch, CURLOPT_URL, $img);
         curl_setopt($ch, CURLOPT_HEADER, 0);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
         $picture = curl_exec($ch);            
         
         // cURL error
         if (curl_error($ch)) {
            $response = array(
         		'error' => true,
         		'msg' => __('cURL Request Error:', 'instant-images') .': '. curl_error($ch),
         		'path' => '',
         		'filename' => ''
      		);
         }  
         
         
         // Success
         else {      
   
            // Save file to server
            $saved_file = file_put_contents($tmp_img, $picture);
      
            // Was the temporary image saved?
            if ($saved_file) {
      
               if(file_exists($path.''.$filename)){
                  
                  //  SUCCESS - Image saved
                  $response = array(
               		'error' => false,
               		'msg' => __('Image successfully uploaded to server.', 'instant-images'),
               		'path' => $path,
               		'filename' => $filename
            		);
            		
               }else{
                  
                  // ERROR - File does NOT exist
                  $response = array(
               		'error' => true,
               		'msg' => __('Uploaded image not found, please ensure you have proper permissions set on the uploads directory.', 'instant-images'),
               		'path' => '',
                     'filename' => ''
            		);
            		
               }
      
            } else {
      
               // ERROR - Error on save
               $response = array(
            		'error' => true,
            		'msg' => __('Unable to download image to server, please check your server permissions of the instant-images folder in your WP uploads directory.', 'instant-images'),
            		'path' => '',
                  'filename' => ''
         		);
      
            }
            
         }
         
         curl_close($ch); // CLose cURL
      }
      
      // cURL not enabled
      else{
         
         $response = array(
      		'error' => true,
      		'msg' => __('cURL is not enabled on your server. Please contact your server administrator.', 'instant-images'),
      		'path' => $path,
      		'filename' => $filename
   		);
   		
      }
      
      wp_send_json($response);      
		
   }

}

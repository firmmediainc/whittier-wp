<?php 
/**
 * The template for displaying basic fields examples
 * Template name: Basic Fields
 */

get_header(); ?>
	<?php get_template_part( 'parts/content','banner');?>	
	<div class="content grid-container">
	
		<div class="inner-content grid-x grid-margin-x grid-padding-x">
	
		    <main class="main small-12 large-8 medium-8 cell" role="main">
				

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
			    	<?php get_template_part( 'parts/loop', 'page' ); ?>
			    
			    <?php endwhile; endif; ?>	
			    
			    <hr>
			    
				<h2>Text Field Example:</h2>
				
				<p><?php 
					$myText = get_field( 'text_field' ); 
					?>
					
				    <?=$myText;?>
				</p>
				
				<hr>
				
				<h2>Textarea Example:</h2>
				<p>
					<?php the_field( 'text_area' ); ?>
				</p>
				
				<hr>
				
				<h2>Number Field Exmple:</h2>
				<p>The number field value is = <?php the_field( 'number_field' ); ?></p>
				
				<hr>
				
				<h2>Range Field:</h2>
				<p>The range field value is = <?php the_field( 'range_field' ); ?></p>
				
				<hr>
				
				<h2>Email Field Value:</h2>
				<p>The email is <?php the_field( 'email_field' ); ?></p>
				
				<hr>
				
				<h2>URL Field:</h2>
				<p>the url is = <?php the_field( 'url_field' ); ?></p>
				
				<hr>
				
				<h2>Password Field:</h2>
				<!-- for this example I will assign the field to a variable so I can use it later in the template -->
				<?php $password = get_field( 'password_field' ); ?>
				<p>Please do not use this on the front end!  The Password is: <?=$password;?></p>
				
						
			    					
			</main> <!-- end #main -->

		    <?php get_sidebar(); ?>
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
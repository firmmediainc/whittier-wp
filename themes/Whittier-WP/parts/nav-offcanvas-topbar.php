<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
// define acf fields as variables for later use
$logo = get_field('logo','option');
$altLogo = get_field('alt_logo','option');

?>

<div class="top-bar" id="top-bar-menu">
	<div class="top-bar-left float-left">
		<ul class="menu">
			<li><a href="<?php echo home_url(); ?>">
				<?php if ($logo):?>
					<img src="<?=$logo['url'];?>" class="nav-logo" alt="<?=$logo['alt'];?>" height="<?=$logo['height'];?>" width="<?=$logo['width'];?>"/>
				<?php else:?>
					<?php bloginfo('name');?>
				<?php endif;?>

				</a>
			</li>
		</ul>
	</div>
	<div class="top-bar-right show-for-medium">
		<?php joints_top_nav(); ?>	
	</div>
	<div class="top-bar-right mobile-menu-toggle float-right show-for-small-only">
		<ul class="menu">
			<li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li>
		</ul>
	</div>
</div>
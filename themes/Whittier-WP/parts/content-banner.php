<?php 
// bg settings
$bannerBG 		 = get_field('banner_bg_color');
$bannerMinHeight = get_field('banner_min-height');	
$useImg 		 = get_field('use_background_image');
$bannerImg 		 = get_field('banner_bg_img');
$bgPosition 	 = get_field('banner_bg_position');
$bgSize 		 = get_field('background_size');
$bgRepeat 		 = get_field('background_repeat');
if ($bgRepeat == 1){
	$repeat = 'repeat';
} else {
	$repeat = 'no-repeat';
}
//banner text settings
$bannerColor 	 = get_field('banner_text_color');
$bannerAlign 	 = get_field('banner_text_align');
?>
<!-- start inline styles added to style tag -->
<style>
	#banner {
		background-color: <?=$bannerBG;?>;
		<?php if ($useImg == 1):?>
		background-image: url(<?=$bannerImg['url']?>);
		background-repeat: <?=$repeat;?>;
		background-position: <?=$bgPosition;?>;
		background-size: <?=$bgSize;?>;
		<?php endif;?>
	}
	#banner header {
		min-height: <?=$bannerMinHeight . 'vh';?>;
		color: <?=$bannerColor;?>;
	}
</style>
<!-- end inline styles added to style tag -->


<section id="banner">
	<header class="article-header grid-x grid-padding-x grid-container align-middle text-<?=$bannerAlign;?>">
		<div class="cell small-12">
			<h1 class="page-title"><?php the_title(); ?></h1>
		</div>
	</header> <!-- end article header -->
</section>
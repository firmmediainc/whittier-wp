<?php
/*
Template Name: Gallery Fields (No Sidebar)
*/

get_header(); ?>
	<?php get_template_part( 'parts/content','banner');?>				
	<div class="content grid-container">
	
		<div class="inner-content grid-x grid-margin-x grid-padding-x">
	
		    <main class="main small-12 medium-12 large-12 cell" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'page' ); ?>
					
				<?php endwhile; endif; ?>
				
				
				
				<?php // set up acf fields data for use in template
				$colsMobile = get_field( 'columns_mobile' );
				$colsTablet = get_field( 'columns_tablet' );
				$colsDesktop = get_field( 'columns_desktop' );
				?>
				
				<?php // start the gallery code here
				$gallery_images = get_field( 'gallery' ); ?>
				<?php if ( $gallery_images ) : ?>
					<!--  update the classes for the grid-x witht eh above data from ACF -->
					<div class="grid-x grid-padding-x grid-margin-y small-up-<?=$colsMobile;?> medium-up-<?=$colsTablet;?> large-up-<?=$colsDesktop;?>">
					<?php foreach ( $gallery_images as $gallery_image ):  ?>
						<div class="cell gallery-thumb">
							<a href="<?php echo $gallery_image['url']; ?>" data-fancybox="gallery">
								<img id="large-image-<?=$gthumb;?>" src="<?php echo $gallery_image['sizes']['thumbnail']; ?>" alt="<?php echo $gallery_image['alt']; ?>" />
							</a>
						</div>
					<?php endforeach; ?>
					</div>
				<?php endif; ?>





			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->

<?php get_footer(); ?>

<?php
/*
Template Name: Repeater Fields (No Sidebar)
*/

get_header(); ?>
	<?php get_template_part( 'parts/content','banner');?>				
	<div class="content grid-container">
	
		<div class="inner-content grid-x grid-margin-x grid-padding-x align-center">
	
		    <main class="main small-12 xmedium-10 cell" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'page' ); ?>
					
				<?php endwhile; endif; ?>
				
				
				<!--  start adding content from ACF repeater fields -->
				<?php if ( have_rows( 'team_members' ) ) : ?>
					<?php while ( have_rows( 'team_members' ) ) : the_row(); ?>
						<?php $img = get_sub_field('photo');?>

							<div class="grid-x grid-padding-x text-center  xmedium-text-left">
								<div class="cell xmedium-3">
									<img class="bioThumbnail" style="width: 100%; height: auto;" src="<?=$img['sizes']['thumbnail'];?>" alt="<?=$img['alt'];?>"/>
								</div>
								<div class="cell xmedium-9">
									<h4><?php the_sub_field( 'name' ); ?></h4>
									<span style="color: <?php the_sub_field( 'favorite_color' ); ?>;">Favorite Color</span>
									<?php the_sub_field( 'biography' ); ?>
								</div>
							</div>
							<hr>

					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found ?>
				<?php endif; ?>
				<!--  end adding content from ACF repeater fields -->


			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->

<?php get_footer(); ?>

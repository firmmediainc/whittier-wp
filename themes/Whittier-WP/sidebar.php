<?php 
/**
 * The sidebar containing the main widget area
 */
$fullWidth = get_field('use_full_width_template');
if($fullWidth){
	$sidebarClass = 'small-12';
} else {
	$sidebarClass = 'small-12 medium-4 large-4';				
}
?>

<div id="sidebar1" class="sidebar <?=$sidebarClass;?> cell" role="complementary">

	<?php if ( is_active_sidebar( 'sidebar1' ) ) : ?>

		<?php dynamic_sidebar( 'sidebar1' ); ?>

	<?php endif; ?>

</div>
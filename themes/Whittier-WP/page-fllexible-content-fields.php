<?php
/*
Template Name: Flexible Content Fields (No Sidebar)
*/

get_header(); ?>
	<?php get_template_part( 'parts/content','banner');?>				
	<div class="content grid-container">
	
		<div class="inner-content grid-x grid-margin-x grid-padding-x align-center">
	
		    <main class="main small-12 xmedium-9 cell" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'page' ); ?>
					
				<?php endwhile; endif; ?>
				
				
				<!-- Start Code to include ACF flexible content fields -->

				<?php if ( have_rows( 'rows' ) ): ?>

					<?php while ( have_rows( 'rows' ) ) : the_row(); ?>
						<?php if ( get_row_layout() == 'full_width' ) : ?>
						<div class="grid-x grid-padding-x align-middle flexible-row">
							<div class="cell small-12 entry-content">
								<?php the_sub_field( 'full_width' ); ?>
							</div>
						</div>
						<?php elseif ( get_row_layout() == 'two_column' ) : ?>
						<div class="grid-x grid-padding-x flexible-row">
							<div class="cell small-12 xmedium-6 entry-content">
								<?php the_sub_field( 'col_one' ); ?>
							</div>
							<div class="cell small-12 xmedium-6 entry-content">							
								<?php the_sub_field( 'col_two' ); ?>
							</div>
						</div>
						<?php elseif ( get_row_layout() == 'custom_column' ) : ?>
						<div class="grid-x grid-padding-x flexible-row <?php the_sub_field('row_classes');?>">	
							<?php if ( have_rows( 'columns' ) ) : ?>
								<?php while ( have_rows( 'columns' ) ) : the_row(); ?>
									<div class="cell entry-content <?php the_sub_field('column_classes');?>">
										<?php the_sub_field( 'column' ); ?>
									</div>
								<?php endwhile; ?>
							<?php else : ?>
								<?php // no rows found ?>
							<?php endif; ?>
						</div>
						<?php elseif ( get_row_layout() == 'horizontal_rule' ) : ?>
						<div class="grid-x grid-padding-x align-middle">
							<div class="cell small-12">
								<hr>
							</div>
						</div>
						<?php endif; ?>
					<?php endwhile; ?>
					</div>
				<?php else: ?>
					<?php // no layouts found ?>
				<?php endif; ?>

			<!-- Start Code to include ACF flexible content fields -->

			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->

<?php get_footer(); ?>

<?php 
/**
 * The template for displaying the kitchen sink
 * Template name: style-guide
 */

get_header(); ?>

<?php // define acf fields as variables
$fullWidth 		= get_field('use_full_width_template');
$noSidebar 		= get_field('no_sidebar');
$useAddContent 	= get_field('use_additional_content');
$addContent 	= get_field('additional_content');
?>	

	<?php get_template_part( 'parts/content','banner');?>

	<div class="content grid-container">
	
		<div class="inner-content grid-x grid-margin-x grid-padding-x">
			<?php if($fullWidth){
				$contentClass = 'small-12';
			} else {
				$contentClass = 'small-12 medium-8';			
			}?>

			   
		    
		    <main class="main <?=$contentClass;?> cell" role="main">
			<!-- start hardcoded foundation kitchen sink for general style guide usage -->
				<!-- Colors ( display color pallate -- edit in /assets/styles/scss/settings.scss ) -->
				<h2>Colors</h2>
				<div class="grid-x small-up-4 medium-up-5 grid-margin-x">
					<div class="cell colorPallet primary">
						<div class="swatchNotes">
							<strong>primary</strong> <br> #1779ba;
						</div>
						<img src="<?php echo get_template_directory_uri() . '/assets/images/swatch-holder.png';?>" alt="swatch-holder" width="50" height="50" />
					</div>
					<div class="cell colorPallet secondary">
						<div class="swatchNotes">
							<strong>secondary</strong> <br> #767676;
						</div>
						<img src="<?php echo get_template_directory_uri() . '/assets/images/swatch-holder.png';?>" alt="swatch-holder" width="50" height="50" />
					</div>
					<div class="cell colorPallet success">
						<div class="swatchNotes">
							<strong>success</strong> <br> #00e4bc;
						</div>						
						<img src="<?php echo get_template_directory_uri() . '/assets/images/swatch-holder.png';?>" alt="swatch-holder" width="50" height="50" />
					</div>
					<div class="cell colorPallet alert">
						<div class="swatchNotes">
							<strong>warning</strong> <br> #ffae00;
						</div>							
						<img src="<?php echo get_template_directory_uri() . '/assets/images/swatch-holder.png';?>" alt="swatch-holder" width="50" height="50" />
					</div>
					<div class="cell colorPallet warning">
						<div class="swatchNotes">
							<strong>alert</strong> <br> #cc4b37;
						</div>
						<img src="<?php echo get_template_directory_uri() . '/assets/images/swatch-holder.png';?>" alt="swatch-holder" width="50" height="50" />
					</div>
					
					<div class="cell colorPallet light-gray">
						<div class="swatchNotes">
							<strong>light-gray</strong> <br> #e6e6e6;
						</div>
						<img src="<?php echo get_template_directory_uri() . '/assets/images/swatch-holder.png';?>" alt="swatch-holder" width="50" height="50" />
					</div>				
					<div class="cell colorPallet medium-gray">
						<div class="swatchNotes">
							<strong>medium-gray</strong> <br> #cacaca;
						</div>
						<img src="<?php echo get_template_directory_uri() . '/assets/images/swatch-holder.png';?>" alt="swatch-holder" width="50" height="50" />
					</div>
					<div class="cell colorPallet dark-gray">
						<div class="swatchNotes">
							<strong>dark-gray</strong> <br> #565656;
						</div>
						<img src="<?php echo get_template_directory_uri() . '/assets/images/swatch-holder.png';?>" alt="swatch-holder" width="50" height="50" />
					</div>	
					<div class="cell colorPallet black">
						<div class="swatchNotes">
							<strong>black</strong> <br> #0a0a0a;
						</div>
						<img src="<?php echo get_template_directory_uri() . '/assets/images/swatch-holder.png';?>" alt="swatch-holder" width="50" height="50" />
					</div>											

										
				</div>
				<hr>
				<!-- Anchors (links) -->
				<h2>Buttons</h2>
				<a href="#0" class="button">Learn More</a>
				<a href="#" class="button secondary">Secondary Button</a>
				<a href="#" class="button success">Success Button</a>
				<a href="#" class="button warning">Warning Button</a>
				<a href="#" class="button alert">Alert Button</a>
				
				<p class="spacer">&nbsp;</p>
				<a href="#0" class="hollow button">Learn More</a>
				<a href="#" class="hollow button secondary">Secondary Button</a>
				<a href="#" class="hollow button success">Success Button</a>
				<a href="#" class="hollow button warning">Warning Button</a>
				<a href="#" class="hollow button alert">Alert Button</a>
				<p class="spacer">&nbsp;</p>
				
				<a class="tiny button" href="#0">So Tiny</a>
				<a class="small button" href="#0">So Small</a>
				<a class="large button" href="#0">So Large</a>
				<a class="expanded button" href="#0">Such Expand</a>
				
				<div class="button-group">
				  <a class="button">One</a>
				  <a class="button">Two</a>
				  <a class="button">Three</a>
				</div>
				
				<hr>
				<h2>Headers</h2>
				
				<h1>h1. This is a very large header.</h1>
				<h2>h2. This is a large header.</h2>
				<h3>h3. This is a medium header.</h3>
				<h4>h4. This is a moderate header.</h4>
				<h5>h5. This is a small header.</h5>
				<h6>h6. This is a tiny header.</h6>

				<hr>

				<h2>Paragraghs</h2>

				<p class="lead"><strong>Lead Paragraph</strong> just add class lead sit amet, consectetur adipisicing elit. Impedit voluptates exercitationem veniam eius similique. Quam ab magni est excepturi quia veniam non voluptatem aut! Expedita impedit, quod dignissimos sunt blanditiis aut voluptatum repellendus atque veniam accusamus id sint natus maxime suscipit nulla tenetur, vero eligendi.</p>
				
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit voluptates exercitationem veniam eius similique. Quam ab magni est excepturi quia veniam non voluptatem aut! Expedita impedit, quod dignissimos sunt blanditiis aut voluptatum repellendus atque veniam accusamus id sint natus maxime suscipit nulla tenetur, vero eligendi.</p>
				
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi necessitatibus repudiandae maxime quod, officia enim aliquam, dolorem cum alias, numquam excepturi nostrum. Ullam praesentium beatae iste, quo eum, ratione, optio quae, minima quaerat aperiam assumenda dolor accusantium quia similique? Mollitia saepe, eligendi accusantium facere laboriosam perferendis dolores maxime repellendus? Ea, modi vitae? Facilis id error quia, dicta, distinctio nihil maxime esse, consequatur quibusdam fugit repellat facere nesciunt sit, blanditiis suscipit ea non dignissimos? Inventore sapiente eveniet commodi in, quia placeat reiciendis accusantium cumque, laboriosam quaerat, nihil fugit ipsam.</p>
				
				<p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit voluptates exercitationem veniam eius similique. Quam ab magni est excepturi quia veniam non voluptatem aut! Expedita impedit, quod dignissimos sunt blanditiis aut voluptatum repellendus atque veniam accusamus id sint natus maxime suscipit nulla tenetur, vero eligendi.</small></p>

				<hr>
								
				<h2>Lists</h2>
				<h4>Ordered List</h4>
				<ul>
					<li>Lorem ipsum dolor sit amet, consectetur, elit. Quas, dolor. </li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
						<ul>
							<li>sublist item 1</li>
							<li>sublist item 2</li>
							<li>sublist item 3</li>
							<li>sublist item 4</li>
						</ul>
					</li>
					<li>Lorem ipsum dolor sit.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </li>
				</ul>
				<h4>With no-bullet Class</h4>
				<ul class="no-bullet">
					<li>Lorem ipsum dolor sit amet, consectetur, elit. Quas, dolor. </li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
						<ul class="no-bullet">
							<li>sublist item 1</li>
							<li>sublist item 2</li>
							<li>sublist item 3</li>
							<li>sublist item 4</li>
						</ul>
					</li>
					<li>Lorem ipsum dolor sit.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum modi deleniti id dicta minus autem beatae fugit vitae a cum.</li>
				</ul>				
				<h4>Ordered Lists</h4>
				<ol>
					<li>Lorem ipsum dolor sit amet, consectetur, elit. Quas, dolor. </li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </li>
					<li>Lorem ipsum dolor sit.
						<ul>
							<li>sublist item 1</li>
							<li>sublist item 2</li>
							<li>sublist item 3</li>
							<li>sublist item 4</li>					
						</ul>
					</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
					<li>Ipsum modi deleniti id dicta minus autem beatae fugit vitae a cum.</li>
				</ol>
				<h4>Defined Lists</h4>
				<dl>
				  <dt>Time</dt>
				  <dd>The indefinite continued progress of existence and events in the past, present, and future regarded as a whole.</dd>
				  <dt>Space</dt>
				  <dd>A continuous area or expanse that is free, available, or unoccupied.</dd>
				  <dd>The dimensions of height, depth, and width within which all things exist and move.</dd>
				</dl>
				<hr>
				
				<?php if ($useAddContent){
					echo $addContent;
				}?>

			<!-- end hardcoded foundation kitchen sink for general style guide usage -->
			    					
			</main> <!-- end #main -->
			<?php if (!$noSidebar):?>
			    <?php get_sidebar(); ?>
			<?php endif;?>
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
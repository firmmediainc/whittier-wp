<?php 
/**
 * The template for displaying content fields examples
 * Template name: Content Fields
 */

get_header(); ?>
	<?php get_template_part( 'parts/content','banner');?>	
	<div class="content grid-container">
	
		<div class="inner-content grid-x grid-margin-x grid-padding-x">
	
		    <main class="main small-12 large-8 medium-8 cell" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    	<?php get_template_part( 'parts/loop', 'page' ); ?>
			    
			    <?php endwhile; endif; ?>
			    
			    <hr>
			    
			    <h2>Image Field:</h2>
			    <?php $image_field = get_field( 'image_field' ); ?>
				<?php if ( $image_field ) { ?>
					<img src="<?php echo $image_field['url']; ?>" alt="<?php echo $image_field['alt']; ?>" />
				<?php } ?>						
			    					
				<hr>
				
				<h2>File Field:</h2>
				<?php $file_field = get_field( 'file_field' ); ?>
				<?php if ( $file_field ) { ?>
					<a href="<?php echo $file_field['url']; ?>"><?php echo $file_field['filename']; ?></a>
				<?php } ?>
				
				<hr>
				
				<h2>Wysiwyg Field:</h2>
				<?php the_field( 'wysiwyg_editor_field' ); ?>

				<hr>
				
				<h2>oEmbed Field</h2>

					<div class="responsive-embed widescreen">
						<?php the_field( 'oembed_field' ); ?>
					</div>	

				<hr>
				
				<h2>Font Awesome Field -- (Addon via Plugin)</h2>
				<?php the_field( 'font_awesome_field' ); ?>
				
			</main> <!-- end #main -->

		    <?php get_sidebar(); ?>
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
<?php 
/**
 * Template name: Relationship Fields
 */

get_header(); ?>
	<?php get_template_part( 'parts/content','banner');?>	
	<div class="content grid-container">
	
		<div class="inner-content grid-x grid-margin-x grid-padding-x">
	
		    <main class="main small-12 large-8 medium-8 cell" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    	<?php get_template_part( 'parts/loop', 'page' ); ?>
			    
			    <?php endwhile; endif; ?>		

			    
<hr>		    

<?php $post_object = get_field( 'post_object_field' ); ?>
<h2>Post Object Field</h2>
<p>This example is returning the_permalink and the_title from a selected post</p>
<?php if ( $post_object ): ?>
	<?php $post = $post_object; ?>
	<?php setup_postdata( $post ); ?> 
		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	<?php wp_reset_postdata(); ?>
<?php endif; ?>

<hr>			    
			    	
<h2>Page Link Field</h2>
<p>This field is returning a page link</p>
<?php the_field( 'page_link_field' ); ?>

<hr>

<h2>Relationship Field</h2>
<p>This field returns the data from multiple pages or posts selected in the page editor.</p>
<p>In this example we are creating a list of selected pages Titles with the permalink to each page as Buttons.</p>

<?php $relationship_field = get_field( 'relationship_field' ); ?>
<?php if ( $relationship_field ): ?>
	<ul class="no-bullet">
	<?php foreach ( $relationship_field as $post ):  ?>
		<?php setup_postdata ( $post ); ?>
			<li><a class="button expanded" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
	<?php endforeach; ?>
	</ul>
<?php wp_reset_postdata(); ?>
<?php endif; ?>

<hr>

<h2>Taxonomy Field</h2>
<p>This field can select data by taxonomy(category)</p>

<?php $taxonomy_field_ids = get_field( 'taxonomy_field' ); ?>
<?php // var_dump( $taxonomy_field_ids ); ?>

<hr>

<h2>User Field</h2>
<p>The user field allows us to select users and use the information from their profiles and information related to them in our templates</p>

<?php $user_field_array = get_field( 'user_field' ); ?> 
<?php // var_dump( $user_field_array ); ?>


<hr>

<h2>Gravity Forms Field</h2>

<p>this is an add-on. If you use Gravity forms this will generated a select field of all forms so we can select a form to use in our code.</p>

<?php $forms_field_object = get_field( 'forms_field' ); ?>
<?=$forms_field_object;?>


			    					
			    					
			</main> <!-- end #main -->

		    <?php get_sidebar(); ?>
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
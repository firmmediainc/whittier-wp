<?php function my_acf_admin_head() {
    ?>
    <style type="text/css">

	/* admin styles */
	@media screen and (max-width: 667px){
		.acf-field[data-width="50"],
		.acf-field-columngroup.acf-field-columngroup.column-layout-1_2  {
			width: 100% !important;
			min-width: 100%;
		}
	}

    </style>
    
    <?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');



function my_acf_collor_pallete_script() {
    ?>
    <script type="text/javascript">
    (function($){
        
        acf.add_filter('color_picker_args', function( args, $field ){

            // do something to args
            args.palettes = ['#2f353e', '#4d5cf1', '#ba36ff','#00e4bc','#ffbc1a','#fd405a', '#fefefe']
            
            console.log(args);
            // return
            return args;
        });
        
    })(jQuery);
    </script>
    <?php
}

add_action('acf/input/admin_footer', 'my_acf_collor_pallete_script');
